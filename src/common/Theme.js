/** @format */
import { DefaultTheme, DarkTheme } from "react-native-paper";

const dark = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
  text: "#ffffff",
    primary: "#1CB5B4",
    accent: "yellow",
    lineColor: "#ffffff",
    background: "#222229", // '#242424' // '#232D4C'
  },
};

const light = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: "#1CB5B4",
    lineColor: "#f9f9f9",
    background: "#ffffff",
    accent: "yellow",
  },
};

export default { dark, light };
